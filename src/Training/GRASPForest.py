from src.Training.GRASPTreeBuilder import GRASPTreeBuilder
from Training.ClassificationMethod import ClassificationMethod
import operator


class GRASPForest(ClassificationMethod):

    GRASPForest = []

    def __init__(self, num_of_trees=None, randomish=None, thresholder=0, prune_thresholder=0.2):
        self.num_of_trees = num_of_trees  # Value of decision
        self.randomish = randomish  # list of data for this node (debug)
        self.thresholder = thresholder
        self.prune_thresholder = prune_thresholder

    def builder(self, dataset):
        self.build_GRASP_Forest(dataset, self.num_of_trees, self.randomish)

    def avaliator(self, dataset):
        return self.use_forest(dataset)

    def build_GRASP_Forest(self, dataset, n, k):

        builder = GRASPTreeBuilder()

        for i in range(0, n):
            grasp_tree = builder.build_tree(dataset, k, self.thresholder)
            builder.prune(grasp_tree, self.prune_thresholder, notify=False)
            self.GRASPForest.append(grasp_tree)

    def use_forest(self, dataset):

        acc = 0
        for row in dataset:
            election = []
            for tree in self.GRASPForest:
                candidate = self.data_classificator(row, tree)
                election.append(candidate)
            winner = max(set(election), key=election.count)
            if winner == row[len(row) - 1]:
                acc += 1
        return acc/len(dataset)

    def data_classificator(self, data, root):

        if root.right is None and root.left is None:
            return max(root.results.items(), key=operator.itemgetter(1))[0]
        else:
            node = root.right
            if isinstance(data[root.col], int) or isinstance(data[root.col], float):
                if data[root.col] < root.value:
                    node = root.left
            else:
                if data[root.col] != root.value:
                    node = root.left
            return self.data_classificator(data, node)
