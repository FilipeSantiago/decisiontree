from src.Training.GreedyTreeBuilder import GreedyTreeBuilder
from Training.ClassificationMethod import ClassificationMethod
import operator


class DecisionTree(ClassificationMethod):

    Tree = object

    def __init__(self, thresholder=0, prune_thresholder=0.2):
        self.thresholder = thresholder
        self.prune_thresholder = prune_thresholder

    def builder(self, dataset):
        builder = GreedyTreeBuilder()
        self.Tree = builder.build_tree(dataset, thresholder=self.thresholder)
        builder.prune(self.Tree, self.prune_thresholder, notify=False)

    def avaliator(self, dataset):
        return self.set_classificator(dataset)

    def data_classificator(self, data, root):

        if root.right is None and root.left is None:
            return root.results
        else:
            node = root.right
            if isinstance(data[root.col], int) or isinstance(data[root.col], float):
                if data[root.col] < root.value:
                    node = root.left
            else:
                if data[root.col] != root.value:
                    node = root.left
            return self.data_classificator(data, node)

    def set_classificator(self, dataset):

        acc = 0
        for data in dataset:
            brute_result = self.data_classificator(data, self.Tree)
            result = max(brute_result.items(), key=operator.itemgetter(1))[0]
            if result == data[len(data) - 1]:
                acc += 1
        return acc / len(dataset)
