from src.Training.GreedyTreeBuilder import GreedyTreeBuilder
from src.Training.GRASPTreeBuilder import GRASPTreeBuilder


class NFoldCrossValidator:

    def cross_validator(self, n, data, classificator):

        split_list = lambda lst, sz: [lst[j:j + sz] for j in range(0, len(lst), sz)]

        part_data = split_list(data, int(len(data)/n))
        total_acc = 0
        for i in range(0, n):
            train_set = []
            validator_set = part_data[i]
            for partial_data in part_data:
                if partial_data != validator_set:
                    train_set += partial_data

            classificator.builder(train_set)
            acc = classificator.avaliator(validator_set)
            total_acc += acc
        return total_acc/n
