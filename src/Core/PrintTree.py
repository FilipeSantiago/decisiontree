from PIL import Image, ImageDraw


class PrintTree:

    def get_width(self, tree):
        if tree.right is None and tree.left is None: return 1
        return self.get_width(tree.right) + self.get_width(tree.left)

    def get_depth(self, tree):
        if tree.right is None and tree.left is None: return 0
        return max(self.get_depth(tree.right), self.get_depth(tree.left)) + 1

    def draw_tree(self, tree, jpeg='tree.jpg'):
        w = self.get_width(tree) * 100
        h = self.get_depth(tree) * 100 + 120

        img = Image.new('RGB', (w, h), (255, 255, 255))
        draw = ImageDraw.Draw(img)

        self.draw_node(draw, tree, w / 2, 20)
        img.save(jpeg, 'JPEG')

    def draw_node(self, draw, tree, x, y):
        if tree.results == None:
            # Get the width of each branch
            w1 = self.get_width(tree.left) * 100
            w2 = self.get_width(tree.right) * 100

            # Determine the total space required by this node
            left = x - (w1 + w2) / 2
            right = x + (w1 + w2) / 2

            # Draw the condition string
            draw.text((x - 20, y - 10), str(tree.col) + ':' + str(tree.value), (0, 0, 0))

            # Draw links to the branches
            draw.line((x, y, left + w1 / 2, y + 100), fill=(255, 0, 0))
            draw.line((x, y, right - w2 / 2, y + 100), fill=(255, 0, 0))

            # Draw the branch nodes
            self.draw_node(draw, tree.left, left + w1 / 2, y + 100)
            self.draw_node(draw, tree.right, right - w2 / 2, y + 100)
        else:
            txt = ''.join(['%s' % v for v in tree.results])
            draw.text((x - 20, y), txt, (0, 0, 0))
