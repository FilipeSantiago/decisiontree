class ArffToMatrix:

    @staticmethod
    def arff_conversion(filename):
        matrix = []

        with open(filename) as fp:
            for line in fp:
                line = line.rstrip('\n')
                array = line.split(',')
                matrix.append(array)

        return matrix
