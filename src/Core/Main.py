from Training.GRASPForest import GRASPForest
from Core.ArffToMatrix import ArffToMatrix
from Training.DecisionTree import DecisionTree
from Training.GreedyTreeBuilder import GreedyTreeBuilder
from Validator.NFoldCrossValidator import NFoldCrossValidator
from random import shuffle
from src.Core.PrintTree import PrintTree
import datetime


class Main:

    # Read Dataset
    dataset_name = "processed.hungarian.data"
    dataset_path = "/home/filipe/PycharmProjects/DecisionTree/datasets/" + dataset_name
    dataset = ArffToMatrix.arff_conversion(dataset_path)
    shuffle(dataset)

    # Training Step
    method1 = GRASPForest(20, 5, thresholder=0.1, prune_thresholder=0.2)
    method12 = GRASPForest(50, 5, thresholder=0.1, prune_thresholder=0.2)
    method2 = DecisionTree(thresholder=0.1, prune_thresholder=0.2)

    drawer = PrintTree()

    # Validator Step
    validator = NFoldCrossValidator()

    print(dataset_name + "\n")

    print("decision tree \n")
    a = datetime.datetime.now()
    acc2 = validator.cross_validator(10, dataset, method2)
    b = datetime.datetime.now()
    delta = b - a
    print(acc2, delta, "\n")

    print("20 tree \n")
    a = datetime.datetime.now()
    acc2 = validator.cross_validator(10, dataset, method1)
    b = datetime.datetime.now()
    delta = b - a
    print(acc2, delta, "\n")

    print("50 tree \n")
    a = datetime.datetime.now()
    acc2 = validator.cross_validator(10, dataset, method12)
    b = datetime.datetime.now()
    delta = b - a
    print(acc2, delta, "\n")


